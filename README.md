# 多个数据库迁移服务(基于laravel)

### 使用方法

`php artisan multiple_database_migrate`

1. 请选择要使用的数据库链接

2. 请选择要进行的操作

    操作和laravel artisan 命令的对应关系

    ```
    run -> migrate
    make -> make:migration 
    rollback -> migrate:rollback
    reset -> migrate:reset
    refresh -> migrate:refresh
    status -> migrate:status
    ``` 
3. 请输入附加参数

    原始命令的附加参数，如：

    `--step=5`
    
    `--table=table`
