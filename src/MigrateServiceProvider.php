<?php

namespace Virchow\MultipleDatabaseMigrate;

use Illuminate\Support\ServiceProvider;
use Virchow\MultipleDatabaseMigrate\Console\Commands\MultipleDatabaseMigrateCommand;

class MigrateServiceProvider extends ServiceProvider
{
    protected $command = [
        MultipleDatabaseMigrateCommand::class
    ];

    public function register()
    {
        $this->commands($this->command);
    }
}
